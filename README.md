# slurm_array

The array job uses the bash shell, sets the job name, the number of cores per machine, the memory required, the job time length, the output and error director(y/ies), and whether to email.

Then you can change to a certain directory and launch a Matlab job in a command line mode that takes the SLURM array task ID as a variable and runs a Matlab script

1. SSH into the head node of the Cluster
2. Put this `ROI_Selection.m` and `array_job.sh` into a folder accessible from the cluster
3. Edit any paths or parameters you want to change
4. do `mkdir output` for logfiles
5. Maybe test a single case (make each of the parameter vectors length 1)
   `sbatch array_job.sh`
   hardcode the parameter `a = 1`
6. launch the big job
   `sbatch --array=1-360 array_job.sh`
7. monitor the job with `squeue -u dpb6` and http://durmstrang.egr.duke.edu/ganglia
8. If you need to cancel the job use `scancel -u dpb6`
