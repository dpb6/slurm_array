% Initialize the workspace
clearvars -except a % keep the variable 'a' which was defined in SLURM
close all
tic

% Constant Variables
env_vs_rf = 1;
    % 1 means you want env_sc, 2 means you want rf data

near_focus_vs_all = 2;
    % 1 means you want the whole image, 2 means you just want near the focus

how_close_to_focus = 20; 
     % if you only want close to focus, this determines how close to the focus to examine

n_mm_ROI = 0.005; % in meters
    % this is the size of the ROI
    
n_mm_shift = 0.005; % in meters
    % this is how much the ROI is shifted during calculations (lower means
    % a higher sampling rate) 
     
types_of_scans = ["placenta" "fetal"]; 
    % these are the types of scans performed in the order they are listed
    % on the naming document 

foldernames = "all"; 
    % these are the patients you want to examine; if you want to look at
    % all avaliable patients on the document, say "all".

filelocation = '/getlab/kmf34/clinical_studies/ALARA_LOC_verasonics_2019/clinical_data/';
    % this determines the filepath for finding the data
    
saveimagelocation = '/getlab/ecb52/ROI_selection_results_round2/';
    % this determines the savepath for saving any printed images

savepathstructs = '/getlab/ecb52/ROI_selection_results_round2/';
    % this determines the savepath for the structs overall 

save_graphing_data = 0;
    % 1 means you want to save the data to be able to quickly graph them
    % later; 0 means you do not. 

% Parameters/Changing Variables 
ath_percentile = [0 10 20 30];
bth_percentile = [90 95 100];
corr_cutoff = [0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
SNR_cutoff = [1.2, 1.3, 1.4, 1.5, 1.6];
% Code for testing the math in the indexing and modulo operators below
%length_a = prod([length(ath_percentile), length(bth_percentile), length(corr_cutoff), length(SNR_cutoff)]);
%a = 1:length_a

% Instead of looping across each of the variables we assign an index 'a'
% for each job in the array. The value of 'a' then determines a unique
% combination of values from the four parameters below.

a_perc = ath_percentile(mod(floor((a-1)/1),length(ath_percentile))+1);
b_perc = bth_percentile(mod(floor((a-1)/length(ath_percentile)),length(bth_percentile))+1);
crr_cut = corr_cutoff(mod(floor((a-1)/length(ath_percentile)/length(bth_percentile)),length(corr_cutoff))+1);
SNR_cut = SNR_cutoff(mod(floor((a-1)/length(ath_percentile)/length(bth_percentile)/length(corr_cutoff)),length(SNR_cutoff))+1);

filter = selectROI(...
                   env_vs_rf,...
                   near_focus_vs_all,...
                   how_close_to_focus,...
                   ath_percentile(a_perc),...
                   bth_percentile(b_perc),...
                   corr_cutoff(crr_cut),...
                   SNR_cutoff(SNR_cut),...
                   n_mm_ROI,...
                   n_mm_shift,...
                   types_of_scans,...
                   foldernames,...
                   filelocation,...
                   saveimagelocation,...
                   savepathstructs,...
                   save_graphing_data);
