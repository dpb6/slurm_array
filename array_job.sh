#!/bin/bash -l
#SBATCH -J test # A single job name for the array
#SBATCH -n 1 # Number of cores
#SBATCH -N 1 # All cores on one machine
#SBATCH --mem 8000 # Memory request
#SBATCH -t 0-23:59 # <24 hours (D-HH:MM)
#SBATCH -o output/test%A%a.out # Standard output
#SBATCH -e output/test%A%a.err # Standard error
#SBATCH --mail-type=END # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=dpb6@duke.edu # Email to which notifications will be sent

cd /tethys/roi_selection/slurm_array

matlab  -nojvm -nodisplay -nodesktop -nosplash -singleCompThread -r 'a=${SLURM_ARRAY_TASK_ID}; pause(10*(a-1)); ROI_Selection; quit'
